# Smart XML parser

This project powered by jsoup xml parser & assembled by gradle

### Features

  - Choose an HTML/XML file with target element id 
  - Choose other sample HTML/XML file with a little bit different content
  - And Smart XML parser will find an element with better matches

***Smart XML parser*** is useful for parsing sites where often but small content changes occur, also candidate competency evaluation


### Running

Before running, make sure you have Java 8 installed

```sh
$ java -jar smart_xml_analyzer.jar <origin_file_path> <other_sample_file_path> [elementId]
```

Where:
  - <origin_file_path> - input origin sample path to find the element with attribute id="make-everything-ok-button" and collect all the required information
  - <other_sample_file_path> - input path to diff-case HTML file to search a similar element
  - [elementId] - not necessary target element id for collecting the initial information through application parameters



### Output

For sample files got the following results:

| File key | Result |
| ------ | ------ |
|sample-0-origin.html|html > body:eq(1) > div > div:eq(1) > div:eq(2) > div > div > div:eq(1) > a.btn.btn-success|
|sample-1-evil-gemini|html > body:eq(1) > div > div:eq(1) > div:eq(2) > div > div > div:eq(1) > a.btn.btn-success|
|sample-2-container-and-clone|html > body:eq(1) > div > div:eq(1) > div:eq(2) > div > div > div:eq(1) > div > a.btn.test-link-ok|
|sample-3-the-escape|html > body:eq(1) > div > div:eq(1) > div:eq(2) > div > div > div:eq(2) > a.btn.btn-success|
|sample-4-the-mash|html > body:eq(1) > div > div:eq(1) > div:eq(2) > div > div > div:eq(2) > a.btn.btn-success|


#### It is works
>Target element can be found with current output strings
>The algorithm is agnostic and flexible to handle cases beyond the provided samples


#### Algorithm
Accordance with point 1 of the section "Must have" and section "Tips & Hints" from the task,
we can use the library to search elements by a standard locator
[link to task](https://agileengine.bitbucket.io/keFivpUlPMtzhfAy/)

1. Find origin element by id
2. Get area path for an element for find target region (two level backward)
3. Found the area in other sample file
4. Collect all tags like origin element tag key
5. Calc attribute match rating for the collection
6. Return element with max attribute matches


