package com.exam.lovyagin.smart_xml_analyzer.test;

import com.exam.lovyagin.smart_xml_analyzer.logic.Worker;
import com.exam.lovyagin.smart_xml_analyzer.model.TagEntity;
import com.exam.lovyagin.smart_xml_analyzer.util.ElementUtil;
import com.exam.lovyagin.smart_xml_analyzer.util.JsoupCssSelectSnippet;
import com.exam.lovyagin.smart_xml_analyzer.util.JsoupFindByIdSnippet;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Arrays;
import java.util.Optional;

import static com.exam.lovyagin.smart_xml_analyzer.config.Configuration.*;

/**
 * Very simple test.
 *
 */
public class SomeTest {
    private final static Logger LOGGER = LoggerFactory.getLogger(SomeTest.class);

    private String absolutePath;
    private Optional<TagEntity> originElement;

    @Before
    public void loadResources() {
        File resourcesDirectory = new File("src/test/resources");
        absolutePath = resourcesDirectory.getAbsolutePath();

        File originFile = new File(absolutePath, DEFAULT_ORIGIN_FILE);
        originElement =
                Optional.of(new TagEntity(JsoupFindByIdSnippet.findElementById(originFile, ORIGIN_ELEMENT_ID)));
    }


    @Test
    public void testGetTargetElement() {

        Arrays.stream(DEFAULT_SAMPLE_FILES).forEach(path ->{
            File file = new File(absolutePath, path);

            Optional<TagEntity> target =
                new Worker(file, originElement.get()).getElementByBestSearchResultInArea();

            // check results
            if (target.isPresent()){
                Optional<Elements> elements =
                    JsoupCssSelectSnippet.findElementsByQuery(file, ElementUtil.getPath(target.get()));


                LOGGER.info(
                        "\nfile name: {} \n{}\n\n",
                        path,
                        ElementUtil.getPath(target.get())
                );

                // only one element should be found for each path
                elements.ifPresent( e ->
                    Assert.assertEquals(e.size(), 1)
                );

            }

        });
    }

}
