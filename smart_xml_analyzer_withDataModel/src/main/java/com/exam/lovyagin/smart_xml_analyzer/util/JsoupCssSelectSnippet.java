package com.exam.lovyagin.smart_xml_analyzer.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import static com.exam.lovyagin.smart_xml_analyzer.config.Configuration.CHARSET_NAME;

/**
 * Class for search elements from htmlFile by cssQuery.
 *
 */
public class JsoupCssSelectSnippet {
    private final static Logger LOGGER = LoggerFactory.getLogger(JsoupCssSelectSnippet.class);

    /**
     * Find element from htmlFile by cssQuery.
     *
     * @param htmlFile
     * @param cssQuery
     * @return
     */
    public static Optional<Elements> findElementsByQuery(File htmlFile, String cssQuery) {
        try {
            Document doc = Jsoup.parse(
                htmlFile,
                CHARSET_NAME,
                htmlFile.getAbsolutePath());

            return Optional.of(doc.select(cssQuery));

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

}