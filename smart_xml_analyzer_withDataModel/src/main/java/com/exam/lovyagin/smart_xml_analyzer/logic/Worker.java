package com.exam.lovyagin.smart_xml_analyzer.logic;

import com.exam.lovyagin.smart_xml_analyzer.model.AttributeEntity;
import com.exam.lovyagin.smart_xml_analyzer.model.TagEntity;
import com.exam.lovyagin.smart_xml_analyzer.util.ElementUtil;
import com.exam.lovyagin.smart_xml_analyzer.util.JsoupCssSelectSnippet;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import static com.exam.lovyagin.smart_xml_analyzer.config.Configuration.PARENT_BACK_LEVEL_TO_FIND_ORIGIN_AREA;

/**
 * Class for a scalability  e.g. multithreading
 * this class contains hi-level business logic
 * for search elements.
 *
 * Also it can be refactored
 *
 */
public class Worker {
    private final static Logger LOGGER = LoggerFactory.getLogger(Worker.class);

    private final File      sampleFile;
    private final TagEntity originElement;


    /**
     * Constructor with DI.
     *
     * @param sampleFile
     * @param originElement
     */
    public Worker( final File sampleFile, final TagEntity originElement) {
        this.sampleFile     = sampleFile;
        this.originElement = originElement;
    }


    /**
     * Get element with max attribute matches.
     *
     * @return
     */
    public Optional<TagEntity> getElementByBestSearchResultInArea(){
        Optional<List<TagEntity>> possibleTargetElements = findAllElementsInArea();

        Optional<TagEntity> tagEntity =
                findElementByCompareAttributes(originElement, possibleTargetElements.orElse(Collections.EMPTY_LIST));

        return tagEntity;
    }


    /**
     * Find element in collection by attributes.
     *
     * @param origin
     * @param possibleTargetElements
     * @return
     */
    private Optional<TagEntity> findElementByCompareAttributes(final TagEntity origin, final List<TagEntity> possibleTargetElements){
        Map<TagEntity, Integer> elementRateMap = new HashMap<>();

        possibleTargetElements.forEach(e ->
            elementRateMap.put(e, getAttributeMatchRating(origin.getAttributes(), e.getAttributes()))
        );

        StringBuilder stringBuilder = new StringBuilder("rate|    tag\n");
        elementRateMap.entrySet().forEach(e->
            stringBuilder.append(e.getValue()).append("\t|").append(e.getKey()).append("\n")
        );

        LOGGER.info("\nCalculate the attribute matching rating for the items " +
                        "and select by the maximum number of matches:\n{}",
                stringBuilder.toString() );

        return elementRateMap.entrySet().stream().max(Comparator.comparing(Map.Entry::getValue)).map(Map.Entry::getKey);
    }


    /**
     * Calc relation rate by attributes.
     *
     * @param originAttributes
     * @param sampleAttributes
     * @return
     */
    private int getAttributeMatchRating(final List<AttributeEntity> originAttributes, final List<AttributeEntity> sampleAttributes){

        Map<String, List<String>> originAttributesMap = new HashMap<>();
        originAttributes.forEach(e ->
            originAttributesMap.put(e.getKey(), e.getValues())
        );

        Map<String,List<String>> sampleAttributesMap = new HashMap<>();
        sampleAttributes.forEach(e ->
            sampleAttributesMap.put(e.getKey(), e.getValues())
        );

        Long rate =
            originAttributes.stream()
                .map(AttributeEntity::getKey)
                .filter(sampleAttributesMap::containsKey)
                .map(k -> sampleAttributesMap.get(k).stream()
                        .filter(originAttributesMap.get(k)::contains)
                        .collect(Collectors.toList())
                )
            .count();

        return rate.intValue();
    }

    /**
     * Find all elements in area
     * @return
     */
    private Optional<List<TagEntity>> findAllElementsInArea(){

        Optional<Element> area = findElementArea();

        Optional<List<TagEntity>> possibleTargetElements = Optional.empty();
        if (area.isPresent()){
            LOGGER.info("\nFind Element area :\n\t{}", ElementUtil.getPath(new TagEntity(area.get())));
            possibleTargetElements = Optional.of(area.get().select(originElement.getName())
                    .stream().map(TagEntity::new).collect(Collectors.toList()));
        }

        return possibleTargetElements;
    }

    /**
     * Find element area to limit the search scope
     * @return
     */
    private Optional<Element> findElementArea(){
        // get origin element area path
        String areaPath = getElementArea(originElement).cssSelector();

        // Accordance with point 1 of the section "Must have" and section "Tips & Hints" from the task,
        // we can use the library to search elements by a standard locator
        Optional<Elements> targetPossibleAreas = JsoupCssSelectSnippet.findElementsByQuery(sampleFile, areaPath);


        if (targetPossibleAreas.isPresent()){
            return Optional.of(targetPossibleAreas.get().first());
        }

        return Optional.empty();
    }

    /**
     * Get area for element for find target region
     * @param child
     * @return
     */
    private TagEntity getElementArea(final TagEntity child){
        TagEntity area = child;

        // get grandparent for element
        for (int i = 0; i < PARENT_BACK_LEVEL_TO_FIND_ORIGIN_AREA; i++){
            if (area.hasParent())
                area = area.getParent();
        }

        return area;
    }
}
