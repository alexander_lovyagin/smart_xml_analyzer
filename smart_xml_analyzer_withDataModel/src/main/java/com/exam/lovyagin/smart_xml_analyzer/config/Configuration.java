package com.exam.lovyagin.smart_xml_analyzer.config;

/**
 * Configuration class
 * Also, properties can load from file or DB if need.
 *
 */
public class Configuration {
    public final static String CHARSET_NAME = "utf8";

    public final static String DEFAULT_ORIGIN_FILE = "sample-0-origin.html";

    public final static String[] DEFAULT_SAMPLE_FILES = {
        "sample-1-evil-gemini.html",
        "sample-2-container-and-clone.html",
        "sample-3-the-escape.html",
        "sample-4-the-mash.html",
    };

    public final static String ORIGIN_ELEMENT_ID = "make-everything-ok-button";

    // affects the limitation of the search area
    public final static int PARENT_BACK_LEVEL_TO_FIND_ORIGIN_AREA = 2;
}
