package com.exam.lovyagin.smart_xml_analyzer;

import com.exam.lovyagin.smart_xml_analyzer.logic.Worker;
import com.exam.lovyagin.smart_xml_analyzer.model.TagEntity;
import com.exam.lovyagin.smart_xml_analyzer.util.ElementUtil;
import com.exam.lovyagin.smart_xml_analyzer.util.JsoupCssSelectSnippet;
import com.exam.lovyagin.smart_xml_analyzer.util.JsoupFindByIdSnippet;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Optional;

import static com.exam.lovyagin.smart_xml_analyzer.config.Configuration.ORIGIN_ELEMENT_ID;

public class Main {
    private final static Logger LOGGER = LoggerFactory.getLogger(Main.class);

    /**
     * MAIN
     * @param args
     */
    public static void main(String[] args) {
        String  errors = "";
        String  originFile;
        String  sampleFile;
        String  elementId;

        if (args.length < 1) {
            errors += "\n\t input_origin_file_path";
        }
        if (args.length < 2) {
            errors += "\n\t input_other_sample_file_path";
        }

        if (!errors.isEmpty()){
            LOGGER.error("\nRequired attribute not found: {}", errors);
            return;
        }
        else {
            originFile  = args[0];
            sampleFile  = args[1];
        }

        elementId = args.length > 2 ? args[2] : ORIGIN_ELEMENT_ID;

        // get origin element
        // Accordance with point 1 of the section "Must have" and section "Tips & Hints" from the task,
        // we can use the library to search elements by a standard locator
        Optional<Element> originElement = JsoupFindByIdSnippet.findElementById(new File(originFile), elementId);

        if (!originElement.isPresent()){
            LOGGER.error("\nOrigin element with id: {} not found in file: {}", elementId, originFile);
            return;
        }

        // find target in sample file
        Optional<TagEntity> target =
                new Worker(
                        new File(sampleFile),
                        new TagEntity(originElement.get())
                ).getElementByBestSearchResultInArea();

        // result control
        if (target.isPresent()){
            Optional<Elements> controlSelect =
                    JsoupCssSelectSnippet.findElementsByQuery(new File(sampleFile), ElementUtil.getPath(target.get()) );

            if (controlSelect.isPresent() && controlSelect.get().size() >1){
                LOGGER.error(
                        "\nAttention, can be found more than one item:\n" +
                        "from file {} : {}", originFile, controlSelect.get().size()
                );
            }
            LOGGER.info("\nselected tag: {}",target.get());
            ElementUtil.printElementPath(target.get());
        }else {
            LOGGER.error("\nTarget element not found in file: {}", originFile);
        }
    }
}
