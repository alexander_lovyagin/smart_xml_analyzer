package com.exam.lovyagin.smart_xml_analyzer.model;

import org.jsoup.nodes.Element;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Entity for able to save on disk, DB or serializable.
 *
 */
public class TagEntity implements Serializable {
    private String name;
    private String text;
    private Set<String> classNames;
    private String cssSelector;
    private TagEntity parent;
    private List<AttributeEntity> attributes;
    private int elementSiblingIndex = 0;

    public TagEntity(Optional<Element> element) {
        this(element.orElse(new Element("empty")));
    }


    public TagEntity (Element element){
        this.name =  element.tagName();
        this.parent = element.hasParent() ? new TagEntity(element.parent()): null;
        this.cssSelector = element.cssSelector();
        this.classNames = element.classNames();
        this.elementSiblingIndex = element.elementSiblingIndex();

        this.attributes = new ArrayList<>();
        element.attributes().forEach(a-> attributes.add(new AttributeEntity(a)));
        this.text = element.text();
    }



    //region Getters & Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TagEntity getParent() {
        return parent;
    }

    public void setParent(TagEntity parent) {
        this.parent = parent;
    }

    public List<AttributeEntity> getAttributes() {
        return attributes;
    }

    public void setAttribute(List<AttributeEntity> attributes) {
        this.attributes = attributes;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean hasParent() {
        return parent!=null;
    }

    public String cssSelector() {
        return cssSelector;
    }

    public Set<String> classNames() {
        return classNames;
    }

    public int elementSiblingIndex() {
        return elementSiblingIndex;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(name).append(" ");
        sb.append(
                String.join(" ",
                        attributes.stream().map(AttributeEntity::toString).collect(Collectors.toList())
                )
        );
        sb.append(">").append(text).append("</").append(name).append(">");

        return sb.toString();
    }

    //endregion
}
