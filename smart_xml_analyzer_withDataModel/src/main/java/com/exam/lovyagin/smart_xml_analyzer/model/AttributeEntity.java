package com.exam.lovyagin.smart_xml_analyzer.model;

import org.jsoup.nodes.Attribute;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Entity for able to save on disk, DB or serializable.
 *
 */
public class AttributeEntity implements Serializable {

    private String key;
    private List<String> values;

    public AttributeEntity() {
    }

    public AttributeEntity(String key, List<String> values) {
        this.key = key;
        this.values = values;
    }

    public AttributeEntity(Attribute attribute) {
        this(attribute.getKey(), Arrays.asList(attribute.getValue().split(" +")));

    }

    //region Getters & Setters

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    //endregion


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(key);
        sb.append("=\"");
        sb.append(String.join(" ", values));
        sb.append("\"");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !obj.getClass().equals(this.getClass()))
            return false;

        AttributeEntity other =  (AttributeEntity) obj;
        return key.equals(other.getKey());
    }
}
