package com.exam.lovyagin.smart_xml_analyzer.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import static com.exam.lovyagin.smart_xml_analyzer.config.Configuration.CHARSET_NAME;

/**
 * Class for search elements from htmlFile by id.
 *
 */
public class JsoupFindByIdSnippet {
    private final static Logger LOGGER = LoggerFactory.getLogger(JsoupFindByIdSnippet.class);

    /**
     * Find element from htmlFile by id.
     *
     * @param htmlFile
     * @param targetElementId
     * @return
     */
    public static Optional<Element> findElementById(File htmlFile, String targetElementId) {
        try {
            Document doc = Jsoup.parse(
                htmlFile,
                CHARSET_NAME,
                htmlFile.getAbsolutePath());

            return Optional.of(doc.getElementById(targetElementId));

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }
}
