package com.exam.lovyagin.smart_xml_analyzer.util;

import com.exam.lovyagin.smart_xml_analyzer.model.TagEntity;

/**
 * Class for utilities.
 *
 */
public class ElementUtil {

    /**
     * Generate element path
     * element.cssSelector() works fine too, but then a path is different with task example.
     *
     *
     * @param element
     * @return
     */
    public static String getPath(final TagEntity element){
        TagEntity elm = element;

        StringBuilder sb = new StringBuilder();

        sb.append(element.getName());
        if (!element.classNames().isEmpty()) {
            sb.append(".");
            sb.append(String.join(".", element.classNames()));
        }

        while (elm.hasParent() && !"html".equals(elm.getName())){
            elm = elm.getParent();
            sb.insert(0," > ");

            if (elm.elementSiblingIndex() > 0) {
                sb.insert(0, ")");
                sb.insert(0, elm.elementSiblingIndex());
                sb.insert(0, ":eq(");
            }

            sb.insert(0, elm.getName());

        }

        return sb.toString();
    }

    /**
     *  Print elements.
     *
     * @param element
     */
    public static void printElementPath(final TagEntity element){
        System.out.println(getPath(element));
        System.out.println();
        System.out.println();
    }


}
